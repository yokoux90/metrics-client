package metrics

import (
	"encoding/json"
	"io"
	"net/http"
	"os"

	"github.com/shirou/gopsutil/v3/cpu"
	"github.com/shirou/gopsutil/v3/disk"
	"github.com/shirou/gopsutil/v3/host"
	"github.com/shirou/gopsutil/v3/mem"
	"github.com/shirou/gopsutil/v3/process"
)

// 监控情报获取
// 绑定请求处理方法
//
// gin:
//
//		 func MetricsHandler(handler http.Handler) gin.HandlerFunc {
//			return func(ctx *gin.Context) {
//				handler.ServeHTTP(ctx.Writer, ctx.Request)
//			}
//	  }
//
//	  r.GET("/metrics", handler.MetricsHandler(metrics.Handler()))
//
// echo:
//
//		func MetricsHandler(handler http.Handler) echo.HandlerFunc {
//			return func(ctx echo.Context) error {
//				handler.ServeHTTP(ctx.Response(), ctx.Request())
//				return nil
//			}
//		}
//
//	 e.GET("/metrics", handler.MetricsHandler(metrics.Handler()))
//
// @return http.HandlerFunc
func Handler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// 获取主机信息
		vHost, _ := host.Info()
		hostInfo := &HostInfo{
			Name:            vHost.Hostname,
			OS:              vHost.OS,
			KernelArch:      vHost.KernelArch,
			KernelVersion:   vHost.KernelVersion,
			Platform:        vHost.Platform,
			PlatformVersion: vHost.PlatformVersion,
		}

		// 获取CPU信息
		vCpuPhysical, _ := cpu.Counts(false)
		vCpuLogical, _ := cpu.Counts(true)
		vCpuDetail, _ := cpu.Info()
		vCpuStatus, _ := cpu.Times(true)
		cpuInfo := &CpuInfo{
			PhysicalCores: vCpuPhysical,
			LogicalCores:  vCpuLogical,
			Detail:        vCpuDetail,
			Status:        vCpuStatus,
		}

		// 获取内存信息
		vMem, _ := mem.VirtualMemory()
		memInfo := &MemInfo{
			Total: vMem.Total,
			Free:  vMem.Free,
			Used:  vMem.UsedPercent,
		}

		vDiskPartitions, _ := disk.Partitions(true)
		vDiskInfo := make([]*DiskInfo, len(vDiskPartitions))

		// 根据分区/挂载点，获取其对应的磁盘信息
		for i, v := range vDiskPartitions {
			info, _ := disk.Usage(v.Mountpoint)
			vDiskInfo[i] = &DiskInfo{
				FileSystem:  info.Fstype,
				Size:        info.Total,
				Used:        info.Used,
				Avail:       info.Free,
				UsedPercent: info.UsedPercent,
				MountPoint:  info.Path,
			}
		}

		// 获取进程信息，获取当前嵌入的目标服务所在进程及其父进程信息
		vProcs, _ := process.Processes()
		vProcInfo := make([]*ProcessInfo, 2)
		for _, ps := range vProcs {
			if ps.Pid == int32(os.Getppid()) {
				user, _ := ps.Username()
				cpuUsage, _ := ps.CPUPercent()
				memUsage, _ := ps.MemoryPercent()
				startTime, _ := ps.CreateTime()
				cmd, _ := ps.Cmdline()
				// 父进程信息
				vProcInfo[0] = &ProcessInfo{
					Type:      "Parent Process",
					User:      user,
					PID:       ps.Pid,
					CpuUsage:  cpuUsage,
					MemUsage:  float64(memUsage),
					StartTime: startTime,
					Cmd:       cmd,
				}
			} else if ps.Pid == int32(os.Getpid()) {
				// 当前进程信息
				user, _ := ps.Username()
				cpuUsage, _ := ps.CPUPercent()
				memUsage, _ := ps.MemoryPercent()
				startTime, _ := ps.CreateTime()
				cmd, _ := ps.Cmdline()
				vProcInfo[1] = &ProcessInfo{
					Type:      "Current Process",
					User:      user,
					PID:       ps.Pid,
					CpuUsage:  cpuUsage,
					MemUsage:  float64(memUsage),
					StartTime: startTime,
					Cmd:       cmd,
				}
			}
		}

		// 封装以上所有信息
		stat := &MetricsResponse{
			Cpu:  cpuInfo,
			Host: hostInfo,
			Mem:  memInfo,
			Disk: vDiskInfo,
			Proc: vProcInfo,
		}
		stat_json, _ := json.Marshal(stat)
		io.WriteString(w, string(stat_json))
	})
}
