package metrics

const MBUnit uint64 = 1024 * 1024
const GBUnit uint64 = 1024 * 1024 * 1024

type HostInfo struct {
	Name            string `json:"name,omitempty"`             // 用户名
	OS              string `json:"os,omitempty"`               // 系统名称
	KernelArch      string `json:"kernel_arch,omitempty"`      // 内核架构
	KernelVersion   string `json:"kernel_version,omitempty"`   // 内核版本
	Platform        string `json:"platform,omitempty"`         // 平台
	PlatformVersion string `json:"platform_version,omitempty"` // 平台版本
}

type CpuInfo struct {
	PhysicalCores int         `json:"physical_cores,omitempty"` // 物理核心数
	LogicalCores  int         `json:"logical_cores,omitempty"`  // 逻辑核心数
	Detail        interface{} `json:"detail,omitempty"`         // 各个核心详细参数
	Status        interface{} `json:"status,omitempty"`         // 各个核心当前使用情报
}

type MemInfo struct {
	Total uint64  `json:"total,omitempty"` // 总内存大小 Bytes
	Free  uint64  `json:"free,omitempty"`  // 可用内存大小 Bytes
	Used  float64 `json:"used,omitempty"`  // 使用率
}

type DiskInfo struct {
	FileSystem  string  `json:"file_system,omitempty"`  // 磁盘存储类型
	Size        uint64  `json:"size,omitempty"`         // 磁盘总容量
	Used        uint64  `json:"used,omitempty"`         // 磁盘已使用容量
	Avail       uint64  `json:"avail,omitempty"`        // 磁盘可用容量
	UsedPercent float64 `json:"used_percent,omitempty"` // 磁盘使用率
	MountPoint  string  `json:"mount_point,omitempty"`  // 挂载点
}

type ProcessInfo struct {
	Type      string  `json:"type,omitempty"`       // 进程类型 父进程/子进程
	User      string  `json:"user,omitempty"`       // 进程当前调用用户
	PID       int32   `json:"pid,omitempty"`        // 进程ID
	CpuUsage  float64 `json:"cpu_usage,omitempty"`  // 进程CPU使用率
	MemUsage  float64 `json:"mem_usage,omitempty"`  // 进程内存使用率
	StartTime int64   `json:"start_time,omitempty"` // 进程开始时间
	Cmd       string  `json:"cmd,omitempty"`        // 进程执行命令
}

type MetricsResponse struct {
	Host *HostInfo      `json:"host,omitempty"`    // 主机信息
	Cpu  *CpuInfo       `json:"cpu,omitempty"`     // CPU信息
	Mem  *MemInfo       `json:"memory,omitempty"`  // 内存信息
	Disk []*DiskInfo    `json:"disk,omitempty"`    // 磁盘信息
	Proc []*ProcessInfo `json:"process,omitempty"` // 进程信息
}
